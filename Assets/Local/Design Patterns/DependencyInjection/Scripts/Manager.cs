﻿using UnityEngine;
using System.Collections;

public class Manager : MonoBehaviour
{
    [SerializeField]
    bool m_IsUnique = true;

    void Awake()
    {
        if (m_IsUnique)
        {
            var managers = FindObjectsOfType<Manager>();
            if (managers.Length > 1)
            {
                Destroy(gameObject);
            }
            else
            {
                DontDestroyOnLoad(gameObject);
            }
        }
    }

    public void DoSomething()
    {

    }
}
