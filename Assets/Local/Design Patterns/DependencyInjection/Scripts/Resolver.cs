﻿using System;
using UnityEngine;

public static class Resolver
{
    public static T ResolveByType<T>()
        where T : Component
    {
        // Prend le premier composant du type trouvé
        throw new NotImplementedException();
    }

    public static T ResolveByTag<T>(string tag)
        where T : Component
    {
        //prend le premier objet portant le tag 'tag' et récupère le bon composant dessus
        throw new NotImplementedException();
    }

    public static T GetOrResolveByTag<T>(T value, string tag)
        where T : Component
    {
        // Si tag est null, fait une résolution par type
        // Sinon, résouds par tag
        throw new NotImplementedException();
    }
}