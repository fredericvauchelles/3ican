﻿using UnityEngine;

public class Singleton2
{
    // Singleton avec instantiation sur l'accès
    private static Singleton2 s_Instance = null;
    public static Singleton2 Instance
    {
        get
        {
            if (s_Instance == null)
            {
                s_Instance = null;
            }
            return s_Instance;
        }
    }

    public void DoSomethinng()
    {
    }
}
