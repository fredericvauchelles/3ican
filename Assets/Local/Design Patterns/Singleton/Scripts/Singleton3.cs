﻿using UnityEngine;

public class Singleton3
{
    // Singleton avec instantiation basée sur Unity
    public static Singleton3 Instance { get; private set; }

    [SerializeField]
    float m_ConfigurableNumber = 1;

    void Awake()
    {
        Instance = this;
    }

    void OnDestroy()
    {
        Instance = null;
    }

    public void DoSomething()
    {
    }
}
