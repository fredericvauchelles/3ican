﻿using UnityEngine;
using System.Collections;

public class PrefabShooter : MonoBehaviour
{
    [Header("Injections")]
    // Mettre les membre pour l'injection

    [Header("Shooter")]
    [SerializeField]
    GameObject m_Prefab = null;
    [SerializeField]
    float m_EmissionDelay = 1;
    [SerializeField]
    float m_LifeDuration = 3;
    [SerializeField]
    Vector3 m_InitialLocalVelocity = Vector3.forward;

    #region Unity
    // Faire l'injection
    // Lancer la coroutine Emit
    #endregion

    IEnumerator Emit()
    {
        while (enabled)
        {
            yield return new WaitForSeconds(m_EmissionDelay);

            // Instancier
            // Parameter le rigidbody
        }
    }

    IEnumerator DestroyAfter(GameObject toDestroy, float delay)
    {
        yield return new WaitForSeconds(delay);

        // Détruire l'objet
    }
}
