﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ObjectPools : MonoBehaviour
{
    [SerializeField]
    List<PoolConfiguration> m_PoolConfiguration = new List<PoolConfiguration>();

    #region API
    public interface IObjectPool
    {
        GameObject Instantiate();
        void Destroy(GameObject toDestroy);
    }

    public IObjectPool this[GameObject prefab]
    {
        get
        {
            // Récuperation du pool
            // Création du pool s'il n'existe pas
        }
    }
    #endregion

    #region Unity
    void Awake()
    {
        // Initialisation des pools
    }
    #endregion

    List<PoolData> m_PoolData = null;

    [Serializable]
    class PoolConfiguration
    {
        public GameObject prefab = null;
        public int initialSize = 10;
        public int growSize = 0;
    }

    class PoolData : IObjectPool
    {
        #region API
        GameObject IObjectPool.Instantiate()
        {
            // Récupère une instance si disponible
            // Sinon agrandi le pool et retourne une instance
        }

        void IObjectPool.Destroy(GameObject toDestroy)
        {
            // Récupère une instance
        }
        #endregion

        public Transform parent = null;
        public PoolConfiguration configuration = null;
        public List<GameObject> instances = null;
        public List<bool> availability = null;

        internal void AddInstancesToPool(int instanceCount)
        {
            // Ajoute n instance au pool
        }

        internal PoolData(PoolConfiguration configuration, Transform parent)
        {
            this.configuration = configuration;
            this.parent = parent;
            this.instances = new List<GameObject>(configuration.initialSize);
            this.availability = new List<bool>(configuration.initialSize);
        }
    }
}
